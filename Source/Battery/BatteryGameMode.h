// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BatteryGameMode.generated.h"

UENUM(BlueprintType)
enum class EBatteryPlayState : uint8
{
	EPlaying,
	EGameOver,
	EWon,
	EUnknown
};

UCLASS(minimalapi)
class ABatteryGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABatteryGameMode();

	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintPure, Category = "Power")
	float GetPowerToWin() const; //Returns power needed to win

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintPure, Category = "Power")
	EBatteryPlayState GetCurrentState() const; //Returns the current playing state

	void SetCurrentState(EBatteryPlayState NewState); //Sets a new playing state

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Power", meta = (BlueprintProtected = "true"))
	float DecayRate; //The rate at which the character loses power

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Power", meta = (BlueprintProtected = "true"))
	float PowerToWin; //The power needed to win the game

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Power", meta = (BlueprintProtected = "true"))
	TSubclassOf<class UUserWidget> HUDWidgetClass; //The widget class to use for our HUD screen

	UPROPERTY()
	class UUserWidget* CurrentWidget; //The actual instance of the HUD

private:
	EBatteryPlayState CurrentState; //Keeps track of the current playing state

	TArray<class ASpawnVolume*> SpawnVolumeActors;

	void FindSpawnVolumeActors();
	void HandleNewState(EBatteryPlayState NewState); //Handle any function calls that rely upon changing the playing state of our game
};