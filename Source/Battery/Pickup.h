// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Pickup.generated.h"

UCLASS()
class BATTERY_API APickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickup();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	FORCEINLINE class UStaticMeshComponent* GetMesh() const //Return the mesh for the pickup
	{
		return PickupMesh;
	}

	UFUNCTION(BlueprintPure, Category = "Pickup")
	bool IsActive(); //Return whether or not the pickup is active

	UFUNCTION(BlueprintCallable, Category = "Pickup")
	void SetActive(bool NewPickupState); //Allows other classes to safely change whether or not the pickup is active

	UFUNCTION(BlueprintNativeEvent)
	void WasCollected(); //Function to call when the pickup is collected
	virtual void WasCollected_Implementation();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	bool bIsActive; //True when the pickup can be used and false when the pickup is deactivated

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pickup", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* PickupMesh; //Static mesh to represent the pickup in the level
};
