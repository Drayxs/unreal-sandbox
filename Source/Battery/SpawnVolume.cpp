// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnVolume.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/BoxComponent.h"
#include "Pickup.h"

// Sets default values
ASpawnVolume::ASpawnVolume()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	WhereToSpawn = CreateDefaultSubobject<UBoxComponent> (TEXT("WhereToSpawn")); //Create the Box Component to represent the spawn volume
	RootComponent = WhereToSpawn;

	SpawnDelayRangeLow = 1.0f; //Setting the spawn delay
	SpawnDelayRangeHigh = 4.5f;
}

// Called when the game starts or when spawned
void ASpawnVolume::BeginPlay()
{
	Super::BeginPlay();	

	//SpawnPickupAfterDelay();
}

// Called every frame
void ASpawnVolume::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

FVector ASpawnVolume::GetRandomPointInVolume()
{
	FVector SpawnOrigin = WhereToSpawn->Bounds.Origin;
	FVector SpawnExtent = WhereToSpawn->Bounds.BoxExtent;

	return UKismetMathLibrary::RandomPointInBoundingBox(SpawnOrigin, SpawnExtent);
}

void ASpawnVolume::SetSpawningActive(bool pShouldSpawn)
{
	if (pShouldSpawn)
	{
		SpawnPickupAfterDelay(); //Start spawning
	}
	else
	{
		GetWorldTimerManager().ClearTimer(SpawnTimer); //Clear the timer
	}
}

void ASpawnVolume::SpawnPickup()
{
	if (WhatToSpawn != NULL) //If we have some something to spawn
	{
		UWorld* const World = GetWorld();

		if (World) //Check for a valid world
		{
			FActorSpawnParameters SpawnParams; //Set the spawn parameters
			SpawnParams.Owner = this;
			SpawnParams.Instigator = Instigator;

			FVector SpawnLocation = GetRandomPointInVolume(); //Get a random location to spawn at			
			FRotator SpawnRotation = UKismetMathLibrary::RandomRotator(); //Get a random rotation for the spawned item
		
			APickup* const SpawnedPickup = World->SpawnActor<APickup>(WhatToSpawn, SpawnLocation, SpawnRotation, SpawnParams); //Spawn the pickup
		
			SpawnPickupAfterDelay();
		}
	}
}

void ASpawnVolume::SpawnPickupAfterDelay()
{
	SpawnDelay = FMath::FRandRange(SpawnDelayRangeLow, SpawnDelayRangeHigh);
	GetWorldTimerManager().SetTimer(SpawnTimer, this, &ASpawnVolume::SpawnPickup, SpawnDelay, false); //Essentially triggers the SpawnPickup function after SpawnDelay
}