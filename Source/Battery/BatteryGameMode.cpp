// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "BatteryGameMode.h"
#include "BatteryCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "SpawnVolume.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Battery.h"

ABatteryGameMode::ABatteryGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	PrimaryActorTick.bCanEverTick = true;

	DecayRate = 0.01f; //Base decay rate
}

void ABatteryGameMode::BeginPlay()
{
	Super::BeginPlay();

	FindSpawnVolumeActors();

	SetCurrentState(EBatteryPlayState::EPlaying);

	ABatteryCharacter* MyCharacter = Cast<ABatteryCharacter>(UGameplayStatics::GetPlayerPawn(this, 0)); //Check that we are using the battery collector character

	if (MyCharacter)
	{
		PowerToWin = (MyCharacter->GetInitialPower()) * 1.25f;
	}

	if (HUDWidgetClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), HUDWidgetClass);

		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}

	
}

void ABatteryGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ABatteryCharacter* MyCharacter = Cast<ABatteryCharacter>(UGameplayStatics::GetPlayerPawn(this, 0)); //Check that we are using the battery collector character

	if (MyCharacter)
	{
		if (MyCharacter->GetCurrentPower() > PowerToWin) //If our power is greather than needed to win, set the game state to won
		{
			SetCurrentState(EBatteryPlayState::EWon);
		}
		else if (MyCharacter->GetCurrentPower() > 0) //If the character's power is positive
		{
			MyCharacter->UpdatePower(-DeltaTime * DecayRate * (MyCharacter->GetInitialPower())); //Decrease the cahracter's power using the decay rate
		}
		else
		{
			SetCurrentState(EBatteryPlayState::EGameOver);
		}
	}
}

float ABatteryGameMode::GetPowerToWin() const
{
	return PowerToWin;
}

EBatteryPlayState ABatteryGameMode::GetCurrentState() const
{
	return CurrentState;
}

void ABatteryGameMode::SetCurrentState(EBatteryPlayState NewState)
{
	CurrentState = NewState;
	HandleNewState(CurrentState);
}

void ABatteryGameMode::FindSpawnVolumeActors()
{
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawnVolume::StaticClass(), FoundActors); //Find all spawn volume actors

	for (auto Actor : FoundActors)
	{
		ASpawnVolume* SpawnVolumeActor = Cast<ASpawnVolume>(Actor);

		if (SpawnVolumeActor)
		{
			SpawnVolumeActors.AddUnique(SpawnVolumeActor);
		}
	}
}

void ABatteryGameMode::HandleNewState(EBatteryPlayState NewState)
{
	switch (NewState)
	{
	case EBatteryPlayState::EPlaying: //If the game is playing
	{
		for (ASpawnVolume* Volume : SpawnVolumeActors)
		{
			Volume->SetSpawningActive(true);
		}

		break;
	}
	case EBatteryPlayState::EWon: //If we've won the game
	{
		for (ASpawnVolume* Volume : SpawnVolumeActors)
		{
			Volume->SetSpawningActive(false);
		}

		break;
	}		
	case EBatteryPlayState::EGameOver: //If we've lost the game
	{
		for (ASpawnVolume* Volume : SpawnVolumeActors)
		{
			Volume->SetSpawningActive(false);
		}

		APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, 0);

		if (PlayerController)
		{
			PlayerController->SetCinematicMode(true, false, false, true, true);
		}

		ACharacter* MyCharacter = UGameplayStatics::GetPlayerCharacter(this, 0);

		if (MyCharacter)
		{
			MyCharacter->GetMesh()->SetSimulatePhysics(true);
			MyCharacter->GetMovementComponent()->MovementState.bCanJump = false;
		}

		break;
	}		
	case EBatteryPlayState::EUnknown: //Unknown state
		break;
	default: //Default state
		break;
	}
}