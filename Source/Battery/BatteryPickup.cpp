// Fill out your copyright notice in the Description page of Project Settings.


#include "BatteryPickup.h"
#include "Classes/Components/StaticMeshComponent.h"

ABatteryPickup::ABatteryPickup()
{
	GetMesh()->SetSimulatePhysics(true);

	BatteryPower = 150.0f; //The base power level of the battery
}

void ABatteryPickup::WasCollected_Implementation()
{
	Super::WasCollected_Implementation(); //Use the base pickup behaviour
	Destroy(); //Destroy the pickup
}

float ABatteryPickup::GetPower() //Report the battery power
{
	return BatteryPower;
}